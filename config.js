module.exports = {
    host: '0.0.0.0',
    port: 3000,
    mongo: {
        host: process.env.MONGO_HOST || 'mongodb://127.0.0.1:27017/attack-db',
        options: {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            ignoreUndefined: true
        }
    },
    redis: {
        // https://github.com/NodeRedis/node_redis#rediscreateclient
        host: process.env.REDIS_HOST || '127.0.0.1',
        port: process.env.REDIS_PORT || '6379',
        connect_timeout: 600000,
        password: process.env.REDIS_PASSWORD || 'starburststream',
    },
    timeZone: 'Asia/Taipei',
    patternFlag: /FLAG{[a-zA-Z0-9_-]+}/g,
    apiSubmitFlag: 'http://localhost/submit',
    secret: require('crypto').randomBytes(40).toString('hex'),
    loginPath: '/login',
    workerTimeout: 10000, // 10,000ms
    cookieMaxAge: 24 * 60 * 60 * 1000 //ms
}