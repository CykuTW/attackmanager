const express = require('express');
const uuid = require('uuid/v4');
const session = require('express-session');
const argon2 = require('argon2');
const cookieParser = require('cookie-parser');
const https = require('https');
const fs = require('fs');
const config = require('./config');


const app = express();

const parseTargets = (targets) => {
    return targets.split('\n')
                .filter(target => !target.trim().startsWith('#'))
                .map(target => {
                    [id, name, host, port] = target.trim().split(/[ \t]+/);
                    return id && name && host && port && {id, name, host, port: parseInt(port)};
                })
                .filter(target => target);
};

app.use(express.urlencoded({ extended: true }))
app.use('/static', express.static('static'));
app.use(cookieParser());
app.use(session({
    secret: config.secret,
    resave: false,
    saveUninitialized: false,
    cookie: { /*secure: true,*/ maxAge: config.cookieMaxAge },
    genid: uuid
}));

app.use((req, res, next) => {
    if (req.path === config.loginPath) {
        next();
    } else {
        let userId = req.session.userId;
        let username = req.session.username;
        if (!userId || !username) {
            return next(Error('404'));
        } else {
            next();
        }
    }
});

app.set('trust proxy', 1);
app.disable('x-powered-by');

app.get(config.loginPath, async (req, res) => {
    res.render('login.ejs');
});

app.post(config.loginPath, async (req, res, next) => {
    const db = req.app.get('db');
    let username = req.body.username.toString();
    let password = req.body.password.toString();
    let user = await db.collection('users').findOne({
        username: username
    });
    if (user) {
        if (await argon2.verify(user.password, password)) {
            req.session.userId = user.id;
            req.session.username = user.username;
            return res.redirect('/');
        } else {
            return next(new Error('404'));
        }
    } else {
        return next(new Error('404'));
    }
});

app.get('/logout', async (req, res, next) => {
    req.session.destroy(err => console.log(err));
    next(new Error('404'));
});

app.get('/', async (req, res) => {
    const db = req.app.get('db');
    const limit = parseInt(req.query.limit) || 10;
    const skip = parseInt(req.query.skip) || 0;

    let attacks = await db.collection('attacks')
                            .find()
                            .limit(limit)
                            .skip(skip)
                            .toArray();
    attacks = await Promise.all(attacks.map(async (item) => {
        delete item._id;
        // bad practice, but save develop time
        let lastAttack = await db.collection('attackHistory').findOne({ id: item.lastAttackHistoryId });
        item.lastAttack = lastAttack;
        return item;   
    }));
    res.render('attack/index.ejs', { attacks });
});

app.get('/attack/add', async (req, res) => {
    const db = req.app.get('db');
    let challenges = await db.collection('challenges').find().toArray();
    challenges = challenges.map(challenge => {
        delete challenge._id;
        return challenge;
    });
    res.render('attack/add.ejs', { challenges });
});

app.post('/attack/add', async (req, res, next) => {
    if (!(['name', 'exploit', 'schedule', 'language', 'challengeId'].every(x => x in req.body))) {
        return next(new Error());
    }

    const db = req.app.get('db');
    const attack = {
        id: uuid(),
        name: req.body.name.toString(),
        exploit: req.body.exploit.toString(),
        schedule: Math.min(60, Math.max(1, parseInt(req.body.schedule) || 1)),
        language: req.body.language.toString(),
        challengeId: req.body.challengeId.toString(),
        lastAttackHistoryId: null
    };

    await db.collection('attacks').insertOne(attack);
    await db.collection('exploitHistory').updateOne({ id: attack.id }, {
        $push: {
            history: {
                name: req.body.name.toString(),
                exploit: req.body.exploit.toString(),
                schedule: Math.min(60, Math.max(1, parseInt(req.body.schedule) || 1)),
                language: req.body.language.toString(),
                challengeId: req.body.challengeId.toString(),
            }
        }
    },  { upsert: true });
    res.render('attack/add-success.ejs');
});

app.get('/attack/:id', async (req, res) => {
    const db = req.app.get('db');
    const outputAsJson = req.query.json;
    let attack = await db.collection('attacks')
                        .findOne({ id: req.params.id.toString() });
    let lastAttack = await db.collection('attackHistory').findOne({ id: attack.lastAttackHistoryId });
    delete attack._id;
    if (lastAttack) {
        delete lastAttack._id;
        attack.lastAttack = lastAttack;
    }
    if (outputAsJson) {
        return res.json(attack);
    }
    let challenges = await db.collection('challenges').find().toArray();
    challenges = challenges.map(challenge => {
        delete challenge._id;
        return challenge;
    });
    res.render('attack/update.ejs', { attack, challenges });
});

app.post('/attack/:id', async (req, res) => {
    if (!(['name', 'exploit', 'schedule', 'language', 'challengeId'].every(x => x in req.body))) {
        return next(new Error());
    }

    const db = req.app.get('db');
    const filter = { id: req.params.id };
    let result = await db.collection('attacks').updateOne(filter, {
        $set: {
            name: req.body.name.toString(),
            exploit: req.body.exploit.toString(),
            schedule: Math.min(60, Math.max(1, parseInt(req.body.schedule) || 1)),
            language: req.body.language.toString(),
            challengeId: req.body.challengeId.toString(),
        }
    });
    await db.collection('exploitHistory').updateOne(filter, {
        $push: {
            history: {
                name: req.body.name.toString(),
                exploit: req.body.exploit.toString(),
                schedule: Math.min(60, Math.max(1, parseInt(req.body.schedule) || 1)),
                language: req.body.language.toString(),
                challengeId: req.body.challengeId.toString(),
            }
        }
    },  { upsert: true });
    res.render('attack/update-success.ejs');
});

app.get('/attack/:id/history', async (req, res) => {
    const db = req.app.get('db');
    const attack = await db.collection('attacks').findOne({ id: req.params.id });
    const histories = await db.collection('attackHistory')
                                .find({ attackId: req.params.id })
                                .sort({ attackTime: -1 })
                                .toArray();
    res.render('attack/history.ejs', { histories, attack });
});

app.get('/history/:id', async (req, res) => {
    const db = req.app.get('db');
    let history = await db.collection('attackHistory').findOne({ id: req.params.id });
    res.render('history/index.ejs', { history });
});

app.get('/challenge', async (req, res) => {
    const db = req.app.get('db');
    let challenges = await db.collection('challenges')
                                .find()
                                .toArray();
    res.render('challenge/index.ejs', { challenges });
});

app.get('/challenge/add', async (req, res) => {
    res.render('challenge/add.ejs');
});

app.post('/challenge/add', async (req, res, next) => {
    if (!(['name', 'targets'].every(x => x in req.body))) {
        return next(new Error());
    }

    const db = req.app.get('db');

    const challenge = {
        id: uuid(),
        name: req.body.name.toString(),
        targets: parseTargets(req.body.targets)
    };
    let result = await db.collection('challenges').insertOne(challenge);
    res.render('challenge/add-success.ejs');
});

app.post('/challenge/:id', async (req, res, next) => {
    if (!(['name', 'targets'].every(x => x in req.body))) {
        return next(new Error());
    }

    const db = req.app.get('db');
    const filter = { id: req.params.id };
    let result = await db.collection('challenges').updateOne(filter, {
        $set: {
            name: req.body.name.toString(),
            targets: parseTargets(req.body.targets)
        }
    });
    res.render('challenge/update-success.ejs');
});

app.get('/datetime', async (req, res) => {
    let currentDate = new Date();
    res.json({
        datetime: currentDate.toLocaleString('zh-TW', {timeZone: 'Asia/Taipei'}),
        timestamp: currentDate.getTime()
    });
});

// errorHandler should be the last one
app.use((err, req, res, next) => {
    console.log(err);
    res.status(404);
    res.header("Server", "Apache");
    res.send(`<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL was not found on this server.</p>
</body></html>
`);
    res.end();
});

const MongoClient = require('mongodb').MongoClient;

// Mongo connection for express
MongoClient.connect(config.mongo.host, config.mongo.options, (err, connection) => {
    if (err) {
        console.error(`Cannot connect to database: ${config.mongo.host}`);
        process.exit();
    }

    const redis = require('redis').createClient(config.redis);
    redis.on('error', err => console.log(err));
    redis.auth(config.redis.password);

    app.set('db', connection.db('attack-db'));
    app.set('redis', redis);
    https.createServer({
        key: fs.readFileSync('self.pem'),
        cert: fs.readFileSync('self.cert')
    }, app).listen(config.port, config.host, () => {
        console.log(`Listening on port ${config.host}:${config.port}!`);
    });
});

// Mongo connection for CronJob
MongoClient.connect(config.mongo.host, config.mongo.options, (err, connection) => {
    if (err) {
        console.error(`Cannot connect to database: ${config.mongo.host}`);
        process.exit();
    }

    const db = connection.db('attack-db');
    const redis = require('redis').createClient(config.redis);
    redis.on('error', err => console.log(err));
    redis.auth(config.redis.password);
    const doCronJob = async () => {
        const executionTime = new Date();

        const strExecutionTime = executionTime.toLocaleString('en-US', {timeZone: 'Asia/Taipei'});
        console.log(`Execution Time: ${strExecutionTime}`);

        let attacks;
        try {
            attacks = await db.collection('attacks')
                                .find()
                                .toArray();
        } catch (err) {
            console.log(err);
            return;
        }

        const isMatchSchedule = schedule => {
            // switch (schedule) {
            //     case '1m':
            //         return true;
            //     case '15m':
            //         return Math.floor(executionTime.getMinutes()/15) === 0;
            //     case '30m':
            //         return Math.floor(executionTime.getMinutes()/30) === 0;
            //     case '1h':
            //         return executionTime.getMinutes() === 0;
            // }
            return Math.floor(executionTime.getMinutes()%schedule) === 0;
        };

        attacks.forEach(async (attack) => {
            if (isMatchSchedule(attack.schedule)) {
                const challenge = await db.collection('challenges').findOne({
                    id: attack.challengeId
                });
                redis.lpush('attackQueue', JSON.stringify({
                    id: attack.id,
                    language: attack.language,
                    exploit: attack.exploit,
                    targets: challenge.targets
                }));
            }
        });
    };

    const CronJob = require('cron').CronJob;
    // every minutes
    new CronJob('0 */1 * * * *', doCronJob, null, true, config.timeZone);
});