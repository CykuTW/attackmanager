const uuid = require('uuid/v4');
const argon2 = require('argon2');
const config = require('./config');


const usage = (example) => {
    console.log('Usage: node cli.js <action> [<Argument1>, <Argument2>..]');
    if (example) console.log('    Example: ' + example);
    process.exit();
}

if (process.argv.length <= 2) {
    usage();
}

const MongoClient = require('mongodb').MongoClient;
MongoClient.connect(config.mongo.host, config.mongo.options, async (err, connection) => {
    if (err) {
        console.error(`Cannot connect to database: ${config.mongo.host}`);
        process.exit();
    }

    const db = connection.db('attack-db');
    if (process.argv[2] == 'adduser') {
        if (process.argv.length < 5) {
            usage('node cli.js adduser my_username my_password');
        }
        await db.collection('users').insertOne({
            id: uuid(),
            username: process.argv[3],
            password: await argon2.hash(process.argv[4])
        });
        connection.close();
    }
});